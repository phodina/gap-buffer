
pub mod gap;

#[cfg(test)]
mod tests {
    use super::gap::GapBuffer;
    
    #[test]
    fn it_works() {

        let mut buf = GapBuffer::new();
        buf.insert_iter("Lord of the Rings".chars());
        buf.set_position(12);
        buf.insert_iter("Onion".chars());
    }
}
