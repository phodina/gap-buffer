use std;
use std::ops::Range;

pub struct GapBuffer<T>{
    storage: Vec<T>,
    gap: Range<usize>
}

impl<T> GapBuffer<T> {
    /// Allocate new GapBuffer
    pub fn new() -> GapBuffer<T> {
        GapBuffer{ storage: Vec::new(), gap: 0..0 }
    }

    /// Return available number of free elements 
    pub fn capacity(&self) -> usize {
        self.storage.capacity()
    }

    /// Return the number of elements currently held
    pub fn len(&self) -> usize {
        self.capacity() - self.gap.len()
    }

    /// Return the insertion position
    pub fn position(&self) -> usize {
        self.gap.start
    }

    /// Return a pointer to the `index`'th element of the underlying storage regardless of the gap
    ///
    /// Safety: `index` must be a valid index into `self.storage`
    unsafe fn space(&self, index: usize) -> *const T {
        self.storage.as_ptr().offset(index as isize)
    }

    /// Return a mutabel pointer to the `index`'th element of the underlying storage regardless of the gap
    ///
    /// Safety: `index` must be a valid index into `self.storage`
    unsafe fn space_mut(&mut self, index: usize) -> *mut T {
        self.storage.as_mut_ptr().offset(index as isize)
    }

    /// Return the offest in the buffer of the `index`, taking the gap into account
    ///
    /// Note: Doesn't check if the index is within range, but never returns an index in the gap
    fn index_to_raw(&self, index: usize) -> usize {
        if index < self.gap.start {
            index
        }
        else {
            index + self.gap.len()
        }
    }

    /// Return a reference to the `index`'th element or None, if out of bounds
    pub fn get(&self, index: usize) -> Option<&T> {
        let raw = self.index_to_raw(index);

        if raw < self.capacity() {
            unsafe {
                Some(&*self.space(raw))
            }
        }
        else {
            None
        }
    }

    /// Set the current position to `pos` and shift the elements.
    ///
    /// Note: If `pos` is out of bounds, panic.
    pub fn set_position(&mut self, pos: usize){
        if pos > self.len() {
            panic!("Index {} out of range for GapBuffer", pos);
        }

        unsafe {
            let gap = self.gap.clone();
            if pos > gap.start {
                let distance = pos - gap.start;
                std::ptr::copy(self.space(gap.end), self.space_mut(gap.start), distance);
            }
            else if pos < gap.start {
                let distance = gap.start - pos;
                std::ptr::copy(self.space(pos), self.space_mut(gap.end - distance), distance);
            }

            self.gap = pos .. pos + gap.len();
        }
    }

    /// Insert `elt` at the current insetion position and increment it after
    pub fn insert(&mut self, elt: T) {
        if self.gap.len() == 0 {
            self.enlarge_gap();
        }

        unsafe {
            let index = self.gap.start;
            std::ptr::write(self.space_mut(index), elt);
        }
        self.gap.start += 1;
    }

    /// Inset the elements produced by `iter` at the current insertion position
    pub fn insert_iter<I>(&mut self, iterable: I)
    where I: IntoIterator<Item=T> {
        for item in iterable{
            self.insert(item)
        }
    }

    /// Remove element just after the insertion position and return it or return None
    pub fn remove(&mut self) -> Option<T> {
        if self.gap.end == self.capacity() {
            return None;
        }

        let element = unsafe { std::ptr::read(self.space(self.gap.end))};

        self.gap.end += 1;
        Some(element)
    }

    /// Double the capacity of the `self.storage`
    fn enlarge_gap(&mut self) {
        let mut new_capacity = self.capacity() * 2;

        if new_capacity == 0 {
            // Existing vector is empty
            new_capacity = 4;
        }

        let mut new = Vec::with_capacity(new_capacity);
        let after_gap = self.capacity() - self.gap.end;
        let new_gap = self.gap.start .. new.capacity() - after_gap;

        unsafe {
            // Move elements before the gap
            std::ptr::copy_nonoverlapping(self.space(0), new.as_mut_ptr(), self.gap.start);
            // Move the elements after the gap
            let new_gap_end = new.as_mut_ptr().offset(new_gap.end as isize);
            std::ptr::copy_nonoverlapping(self.space(self.gap.end), new_gap_end, after_gap);
        }

        // Drop the old Vec
        self.storage = new;
        self.gap = new_gap;
    }
}

impl<T> Drop for GapBuffer<T> {
    fn drop(&mut self) {
        unsafe {
            for i in 0 .. self.gap.start {
                std::ptr::drop_in_place(self.space_mut(i));
            }
            for i in self.gap.end .. self.capacity() {
                std::ptr::drop_in_place(self.space_mut(i));
            }
        }
    }
}
