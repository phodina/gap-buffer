# Gap buffer

Buffer that allows you to insert or delete characters at constant speed just like Emacs text editor does.
